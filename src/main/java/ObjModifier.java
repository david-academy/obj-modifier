import java.nio.FloatBuffer;

public class ObjModifier {
    public static void centerVertices(FloatBuffer buf) {
        // We often get 3D models which are not positioned at the origin of the coordinate system. Placing such models
        // on a 3D map is harder for users. One solution is to reposition such a model so that its center is at the
        // origin of the coordinate system. Your task is to implement the centering.
        //
        // FloatBuffer stores the vertices of a mesh in a continuous array of floats (see below)
        // [x0, y0, z0, x1, y1, z1, ..., xn, yn, zn]
        // This kind of layout is common in low-level 3D graphics APIs.
        // TODO: Implement your solution here
        float[] floats = new float[buf.capacity()];

        buf.get(floats);


        float[] sums = new float[3];
        float[] avgs = new float[3];


        //SUM
        for (int i = 0 ; i < floats.length; i+=3) {
            sums[0] += floats[i];       //X
            sums[1] += floats[i+1];     //Y
            sums[2] += floats[i+2];     //Z
        }
        System.out.println("SUM");
        System.out.println("X: " + sums[0]);
        System.out.println("Y: "+ sums[1]);
        System.out.println("Z: " + sums[2]);


        //AVERAGE
        for (int i = 0; i < avgs.length; i++) {
            avgs[i] = sums[i]/(floats.length/3);
        }
        System.out.println("--------------------");
        System.out.println("AVERAGE");
        System.out.println("X: " + avgs[0]);
        System.out.println("Y: "+ avgs[1]);
        System.out.println("Z: "+ avgs[2]);

        //ADJUSTING
        for (int i = 0 ; i < floats.length; i+=3) {
            floats[i] -= avgs[0];       //X
            floats[i+1] -= avgs[1];     //Y
            floats[i+2] -= avgs[2];     //Z

        }

        for (int i = 0; i < floats.length; i++) {
            buf.put(i, floats[i]);
        }

    }
}
